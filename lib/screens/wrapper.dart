import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mental_health/screens/authenticate/authenticate.dart';
import 'package:mental_health/screens/home/home.dart';
import 'package:provider/provider.dart';
class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    //Return either Home or Authenticate
    final firebaseUser = context.watch<User>();
    if(firebaseUser == null) return Authenticate();

    return Home();
  }
}
