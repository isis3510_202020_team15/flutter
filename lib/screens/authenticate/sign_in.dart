import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mental_health/screens/authenticate/register.dart';
import 'package:mental_health/screens/home/home.dart';
import 'package:mental_health/services/auth.dart';

class SignIn extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => new _SignInState();
}

class _SignInState extends State<SignIn> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final AuthService _auth = AuthService();
  dynamic images=["https://musculardystrophynews.com/wp-content/uploads/2018/07/shutterstock_282943754-760x475@2x.jpg",
    "https://cdn.pixabay.com/photo/2016/11/29/05/45/astronomy-1867616__340.jpg",
    "https://wallpaperaccess.com/full/1251284.jpg",
  "https://www.incimages.com/uploaded_files/image/1920x1080/getty_669687786_371752.jpg"];
  dynamic url = "https://musculardystrophynews.com/wp-content/uploads/2018/07/shutterstock_282943754-760x475@2x.jpg";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('Mental Health'),
        ),
        body: Column(
          children: [
            Padding(
                padding: EdgeInsets.all(10),
                child: ListView(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'Log in',
                          style: TextStyle(
                              color: Colors.lightBlueAccent,
                              fontWeight: FontWeight.w500,
                              fontSize: 30),
                        )),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        controller: emailController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(10.0),
                              )),
                          labelText: 'Email',
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      child: TextField(
                        obscureText: true,
                        controller: passwordController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(10.0),
                              )),
                          labelText: 'Password',
                        ),
                      ),
                    ),
                    FlatButton(
                      onPressed: (){
                        //forgot password screen
                      },
                      textColor: Colors.lightBlueAccent,
                      child: Text('Forgot Password'),
                    ),
                    Container(
                        height: 50,
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: RaisedButton(
                          textColor: Colors.white,
                          color: Colors.lightBlueAccent,
                          child: Text('Log in'),
                          onPressed: () async {
                            print(emailController.text);
                            print(passwordController.text);
                            dynamic result = await _auth.signIn(
                                email: emailController.text,
                                password: passwordController.text
                            );
                          },
                        )),
                    Container(
                        child: Row(
                          children: <Widget>[
                            Text('Does not have account?'),
                            FlatButton(
                              textColor: Colors.lightBlueAccent,
                              child: Text(
                                'Sign up',
                                style: TextStyle(fontSize: 20),
                              ),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => Register()),
                                );
                              },
                            )
                          ],
                          mainAxisAlignment: MainAxisAlignment.center,
                        ))
                  ],
                )
            ),
            Container(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    final _random = new Random();

                    var element = images[_random.nextInt(images.length)];
                    print(element);
                    url = element;
                  });
                }, // handle your image tap here
                child: Image(
                  image: CachedNetworkImageProvider(url),
                  fit: BoxFit.cover, // this is the solution for border
                  width: 110.0,
                  height: 110.0,
                ),
              )
            )
          ]
        ));
  }
}