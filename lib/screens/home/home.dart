import 'dart:async';
import 'dart:io';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mental_health/components/my_card.dart';
import 'package:mental_health/screens/home/images_and_faces.dart';
import 'package:mental_health/screens/home/mood_history.dart';
import 'package:mental_health/services/auth.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool recommendSport;
  File _imageFile;
  List<Face> _faces;
  Position _lastPos;
  double _distance;
  FlutterLocalNotificationsPlugin _notification;
  StreamSubscription<Position> _positionStream;
  GeolocatorPlatform _gp = GeolocatorPlatform.instance;

  Future _showNotification() async {
      print('show notification');
      var androidDetails = AndroidNotificationDetails('Channel ID', 'Mental Health', 'ping', importance: Importance.max);
      var generalNotificationDetails = NotificationDetails(android: androidDetails);

      await _notification.show(0, 'Out of home?', "Why don't you start running?", generalNotificationDetails);
      print(_notification);
  }
  Future notificationSelected(String payload) async {

  }

  Future _getImageAndDetectFaces (imageFile) async {
    final image = FirebaseVisionImage.fromFile(imageFile);
    FaceDetectorOptions options = FaceDetectorOptions(mode: FaceDetectorMode.accurate, enableLandmarks: true, enableClassification: true);
    FaceDetector faceDetector = FirebaseVision.instance.faceDetector(options);
    final faces = await faceDetector.processImage(image);
    print(faces);
    print(imageFile);
    if(mounted) {
      setState(() {
        _imageFile = imageFile;
        _faces = faces;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    recommendSport = false;
    _distance = 0;
    InitializationSettings options = InitializationSettings(android: AndroidInitializationSettings('app_icon'));
    _notification = FlutterLocalNotificationsPlugin();
    _notification.initialize(options);

    try{
      _positionStream = _gp
          .getPositionStream(desiredAccuracy: LocationAccuracy.high, distanceFilter: 10, timeInterval: 15)
          .listen((position) {
            print(position);
            print(_lastPos);
            if(_lastPos == null){
              setState(() {
                _lastPos = position;
              });
            }
            double distanceBetween = _gp.distanceBetween(position.latitude, position.longitude, _lastPos?.latitude, _lastPos?.longitude);
            print(distanceBetween);
            if(distanceBetween > 100){
              // _showNotification();
              setState(() {
                recommendSport = true;
                _lastPos = position;
                if(position.speed <= 15) {
                  _distance += distanceBetween;
                }
              });
            }
          });

      _positionStream.onError((error) {
        print(error);
      });
    }catch(e){
      print(e.toString());
    }

  }

  @override
  void dispose(){
    super.dispose();
    _positionStream.cancel();
  }

  @override
  Widget build(BuildContext context) {
    AuthService _auth = AuthService();

    return Scaffold(
      resizeToAvoidBottomInset: false,
        appBar: AppBar(
            backgroundColor: Colors.lightBlueAccent,
            title: Align(
              alignment: Alignment.centerRight,
              child: FlatButton(
                child: Text('Sign out'),
                textColor: Colors.white,
                onPressed: () async {
                  await _auth.signOut();
                },
              ),
            ),
        ),
        body:  ListView(
          children: <Widget>[
            MyCard(
                name: 'Upload Photo',
                subtitle: 'Get your current mood',
                width: null,
                open: true,
                callback: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ImagesAndFaces(imageFile: _imageFile, faces: _faces)),
                ),
            ),
            MyCard(
              name: 'Distance per day',
              subtitle: '${_distance==null? 0 : _distance} meters',
              width: null,
              open: false
            ),
            MyCard(
                name: 'My Mood',
                subtitle: "We've track your mood with charts!",
                width: null,
                open: true,
              callback: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MoodHistory()),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Text('Articles')
            ),
            MyCard(
              name: 'Mental Health and the Covid-19 Pandemic',
              subtitle: 'https://www.nejm.org/doi/full/10.1056/NEJMp2008017',
              width: null,
              open: true,
            ),
            MyCard(
              name: 'Keep your heart and brain young',
              subtitle: 'https://www.sharecare.com/health/aging-and-fitness/article/want-keep-heart-brain-young',
              width: null,
              open: true,
            )
          ]
        ),
    );
  }
}


