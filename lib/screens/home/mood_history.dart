import 'dart:async';
import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mental_health/services/auth.dart';
import 'package:mental_health/services/storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:toast/toast.dart';

class MoodHistory extends StatefulWidget {
  @override
  _MoodHistoryState createState() => _MoodHistoryState();
}

class _MoodHistoryState extends State<MoodHistory> {

  Map<String, dynamic> moodData;
  StorageService _storage = StorageService();
  List<DataObject> objects;
  int happy;
  int sad;
  int normal;
  bool loading;

  Future<void> getData()  async {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      AuthService _auth = AuthService();
      User user = _auth.getCurrentUser();
      List<DataObject> cur = new List<DataObject>();
      List<String> curDates = new List<String>();
      List<double> curSmiles = new List<double>();
      List<int> curTypes = new List<int>();

    for(String key in _prefs.getKeys() ){
      print(key);
        if(key.contains(user.uid + "-date")){
          String dateToStringFormat = _prefs.getString(key);
          curDates.add(dateToStringFormat);
        } else if(key.contains(user.uid + "-smile")){
          double smile = _prefs.getDouble(key);
          curSmiles.add(smile);
        } else if(key.contains(user.uid + "-type")){
          int type = _prefs.getInt(key);
          curTypes.add(type);
        }
    }

    for(int i = 0; i < curDates.length; i++){
      cur.add(DataObject(curSmiles[i], curDates[i]));
    }
    int curHappy = _prefs.get(user.uid + "-normal");
    int curSad = _prefs.get(user.uid + "-sad");
    int curNormal = _prefs.get(user.uid + "-normal");
    Toast.show("This is an outdated data", context, duration: Toast.LENGTH_LONG);
    setState(() {
      objects = cur;
      happy = curHappy == null ? 0 : curHappy;
      sad = curSad == null ? 0 : curSad;
      normal = curNormal == null ? 0 : curNormal;
    });
  }

  @override
  void initState() {

    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    print("INIT STATE");
  }

  bool isConnected;
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;



  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        setState(() => isConnected = true);
        _storage.getFaceData().then((value) => setState(() {
          objects = value["data"];
          happy = value["happy"];
          sad = value["sad"];
          normal = value["normal"];
        })
        );
        break;
      default:
        setState(() =>
        isConnected = false
        );
        getData();
        break;
    }
  }

  @override
  Widget build(BuildContext context)  {
    print(happy);
    AuthService _auth = AuthService();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent,
        title: Align(
          alignment: Alignment.centerRight,
          child: FlatButton(
            child: Text('Sign out'),
            textColor: Colors.white,
            onPressed: () async {
              await _auth.signOut();
            },
          ),
        ),
      ),
      body: happy==null || sad == null || normal == null || happy+sad+normal == 0 ? Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            Icon(Icons.add_to_home_screen),
            Text("Upload your photos first!"),
          ],
        )
      ): ListView(
          children: [
            Align(
              alignment: Alignment.center,
              child: !isConnected? Align(
                alignment: Alignment.center,
                child: Column(
                    children: [
                      Icon(Icons.wifi_off),
                      Text("Data shown is outdated, please connect to wifi!")
                    ]
                ),
                ) : Text("Everything up to date!"),
            ),
            SfCartesianChart(
              primaryXAxis: CategoryAxis(),

              series: <ChartSeries<DataObject, String>>[
                LineSeries<DataObject, String>(
                  // Bind data source
                  dataSource: objects,
                  xValueMapper: (object, _) => object.date*100,
                  yValueMapper: (object, _) => object.smiling,

                )
              ]
            ),
            SfCircularChart(
              // Enables the tooltip for all the series in chart
                tooltipBehavior: TooltipBehavior(enable: true),
                legend: Legend(isVisible: true),
                series: <CircularSeries>[
                  PieSeries<_PieObject, String>(
                    // Enables the tooltip for individual series
                      enableTooltip: true,
                      dataSource: [
                        _PieObject(Colors.greenAccent, happy, "happy"),
                        _PieObject(Colors.amberAccent, normal, "normal"),
                        _PieObject(Colors.redAccent, sad, "sad"),
                      ],
                      pointColorMapper: (_PieObject sales, _) => sales.color,
                      xValueMapper: (_PieObject sales, _) => sales.name,
                      yValueMapper: (_PieObject sales, _) => sales.value
                  )
                ],
            )
          ]
      ),
    );
  }
}

class _PieObject{
  _PieObject(this.color, this.value, this.name);
  final Color color;
  final int value;
  final String name;
}


