import 'dart:async';
import 'dart:ffi';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mental_health/services/auth.dart';
import 'package:mental_health/services/storage.dart';
import 'package:mental_health/services/visual_ml.dart';
import 'package:toast/toast.dart';

class ImagesAndFaces extends StatefulWidget {

  ImagesAndFaces({this.imageFile, this.faces});
  File imageFile;
  List<Face> faces;

  @override
  _ImagesAndFacesState createState() => _ImagesAndFacesState();
}

class _ImagesAndFacesState extends State<ImagesAndFaces> {
  File imageFile = ImagesAndFaces().imageFile;
  List<Face> faces = ImagesAndFaces().faces;
  Future _getImageAndDetectFaces (_imageFile) async {
    final image = FirebaseVisionImage.fromFile(_imageFile);
    print(image);
    FaceDetectorOptions options = FaceDetectorOptions(mode: FaceDetectorMode.accurate, enableLandmarks: true, enableClassification: true);
    FaceDetector faceDetector = FirebaseVision.instance.faceDetector(options);
    final _faces = await faceDetector.processImage(image);
    print(_faces);
    print(_imageFile);
    if(mounted) {
      setState(() {
        imageFile = _imageFile;
        faces = _faces;
      });
    }
  }
  bool isConnected = true;
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        setState(() => isConnected = true);
        break;
      default:
        setState(() =>
          isConnected = false
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    AuthService _auth = AuthService();
    StorageService _storage = StorageService();
    String dropdownValue = 'Camera';
    FaceRecognition _fr = FaceRecognition();
    print(isConnected);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent,
        title: Align(
          alignment: Alignment.centerRight,
          child: FlatButton(
            child: Text('Sign out'),
            textColor: Colors.white,
            onPressed: () async {
              await _auth.signOut();
            },
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
              flex: 1,
              child: Container(
                  constraints: BoxConstraints.expand(),
                  child: imageFile==null? Container() : Image.file(
                    imageFile,
                    fit: BoxFit.cover,
                  )
              )
          ),
          Flexible(
              flex: 1,
              child: faces==null? Container() : ListView(children: faces.map<Widget>((f) => FaceCoordinates(f)).toList(),)
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: "Pick an image",
          onPressed: (){},
          child: DropdownButton<String>(
            value: dropdownValue,
            elevation: 16,
            onChanged: (String newValue) {
              setState(() {
                dropdownValue = newValue;
              });
            },
            items: <String>['Camera', 'Gallery']
                .map<DropdownMenuItem<String>>((String value) {
              if(value.compareTo('Camera') == 0){
                return DropdownMenuItem<String>(
                  value: value,
                  child: Icon(Icons.photo_camera),
                  onTap: () async {
                    if(!isConnected) return Toast.show("Service require internet connection", context);
                    dynamic result = await _fr.pickFromCamera();
                    print(result);
                    _getImageAndDetectFaces(result);
                    print(_storage.uploadFile(result));
                  },
                );
              }
              return DropdownMenuItem<String>(
                value: value,
                child: Icon(Icons.camera_roll),
                onTap:() async {
                  if(isConnected){
                  dynamic result = await _fr.pickFromGallery();
                  _getImageAndDetectFaces(result);
                  } else {
                    Toast.show("Service require internet connection", context);
                  }
                },
              );
            }).toList(),
          )
      ),
    );
  }
}

class FaceCoordinates extends StatelessWidget {
  FaceCoordinates(this.face);
  final Face face;
  @override
  Widget build(BuildContext context) {
    StorageService _storage = StorageService();
    _storage.uploadFaceData(face);
    final pos = face.boundingBox;
    return Column(
        children: <Widget>[
          Text('(${pos.top}, ${pos.left}), (${pos.bottom}, ${pos.right})', textScaleFactor: 1.5, textAlign: TextAlign.left),
          Text('Smiling Probability: ${face.smilingProbability.toStringAsPrecision(3)}%', textScaleFactor: 1.5, textAlign: TextAlign.left),
          Text('Left Eye Open Probability: ${face.leftEyeOpenProbability.toStringAsPrecision(3)}%', textScaleFactor: 1.5, textAlign: TextAlign.left,),
          Text('Right Eye Open Probability: ${face.rightEyeOpenProbability.toStringAsPrecision(3)}%', textScaleFactor: 1.5, textAlign: TextAlign.left),
        ]

    );
  }
}