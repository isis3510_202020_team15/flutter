import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';

class AuthService{

  final FirebaseAuth _auth = FirebaseAuth.instance;

  User _userFromFirebaseUser(user) {
    return user;
  }

  User getCurrentUser(){
    return _auth.currentUser;
  }

  Stream<User> get authStateChanges => _auth.authStateChanges();


  //Sign in anon
  //Future = async
  Future signInAnon() async {
    try{
      UserCredential result = await _auth.signInAnonymously();
      User user = result.user;
      return user;
    } catch(e){
      print(e.toString());
      return null;
    }
  }
  //Sign in email password
  Future signIn({String email, String password}) async{
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(email: email, password: password);
      print(userCredential.user);
      return userCredential.user;
    } on FirebaseAuthException catch(e){
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
      return null;
    } catch(e){
      print(e.toString());
      return null;
    }
  }

  //Register email password
  Future register({String email, String password, String repeatedPassword}) async{
    try {
      if(password.compareTo(repeatedPassword) != 0) throw new Exception("Passwords are different");
      UserCredential userCredential = await _auth.createUserWithEmailAndPassword(
          email: email,
          password: password
      );
      print(userCredential.user);
      return userCredential.user;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      } else{
        print(e.toString());
      }
      return null;
    } catch(e){
      print(e.toString());
      return null;
    }
  }
  //Sign in google

  //Sign out
  Future signOut() async {
    await _auth.signOut();
  }
}