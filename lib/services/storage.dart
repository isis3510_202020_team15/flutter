import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';
import 'package:mental_health/services/auth.dart';
import 'package:shared_preferences/shared_preferences.dart';


class StorageService{

  FirebaseStorage _storage = FirebaseStorage.instance;
  FirebaseFirestore db = FirebaseFirestore.instance;
  AuthService _auth = AuthService();

  bool uploadFile(File file) {
    //Address
    try {
      String child = file.path.replaceAll('/storage/emulated/0/Android/data/com.moviles.mental_health/files/', '');
      _storage
          .ref()
          .child(child)
          .putFile(file);
      return true;
    } on FirebaseException catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<Map<String, dynamic>> getFaceData() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();

    User user = _auth.getCurrentUser();
    QuerySnapshot value = await db.collection("users")
      .where("uid", isEqualTo: user.uid)
      .get();

    if(value == null || value.docs.isEmpty){
      return null;
    }
    Map<String, dynamic> data = value.docs.first.data();
    List<DataObject> objects = new List<DataObject>();
    int happy = 0, sad = 0, normal = 0;
    for(int i = 0; i < data["smilings"].length; i++){
      String keyPrefDate = user.uid + "-date-" + i.toString();
      String keyPrefSmile = user.uid + "-smile-" + i.toString();

      Timestamp timestamp = data["dates"][i];
      String date = DateTime.fromMillisecondsSinceEpoch(timestamp.millisecondsSinceEpoch).toString();
      await _prefs.setString(keyPrefDate, date);
      double smile = data["smilings"][i];
      await _prefs.setDouble(keyPrefSmile, smile);
      if(smile*100 >= 90) happy++;
      else if(smile*100 > 10) normal++;
      else sad++;

      objects.add(DataObject(smile,  date));
    }
    await _prefs.setInt(user.uid + "-happy", happy);
    await _prefs.setInt(user.uid + "-sad", sad);
    await _prefs.setInt(user.uid + "-normal", normal);
    return {"data": objects, "happy": happy, "sad": sad, "normal": normal};
  }




  Future<void> uploadFaceData(Face face) async {
    //Address
    try {
      print(face.rightEyeOpenProbability);
      print(face.leftEyeOpenProbability);
      print(face.smilingProbability);
      User user = _auth.getCurrentUser();
      QuerySnapshot value = await db.collection("users")
          .where("uid", isEqualTo: user.uid)
          .get();
      
      List<double> currentFaceData = [face.leftEyeOpenProbability, face.smilingProbability, face.rightEyeOpenProbability];
      DateTime currentDate = DateTime.now();
      if(value.docs.isEmpty){
        await db.collection("users")
            .add({
          "uid": user.uid,
          "leftEyes": [face.leftEyeOpenProbability],
          "rightEyes": [face.rightEyeOpenProbability],
          "smilings": [face.smilingProbability],
          "dates": [currentDate]
        });
        return;
      }
      QueryDocumentSnapshot qds = value.docs.first;
      Map<String, dynamic> obj = qds.data();
      List<dynamic> smilings = [];
      List<dynamic> leftEyes = [];
      List<dynamic> rightEyes = [];
      List<dynamic> dates = [];

      for(MapEntry<String,dynamic> entry in obj.entries){
        if(entry.key == "smilings") smilings = entry.value;
        else if(entry.key == "leftEyes") leftEyes = entry.value;
        else if(entry.key == "rightEyes") rightEyes = entry.value;
        else if(entry.key == "dates") dates = entry.value;

      }
      print(qds.id);
      smilings.add(face.smilingProbability);
      leftEyes.add(face.leftEyeOpenProbability);
      rightEyes.add(face.rightEyeOpenProbability);
      dates.add(currentDate);

      await db.collection("users").doc(qds.id).set({
         "smilings": smilings,
          "leftEyes": leftEyes,
          "rightEyes": rightEyes,
          "dates": dates,
          "uid": user.uid,
      });
    } on FirebaseException catch (e) {
      print(e.toString());
    }
  }
  

  Future<String> getURL(File file) async {
    return await _storage.ref().child('documents/').getDownloadURL();
  }

}

class DataObject {
  DataObject(this.smiling, this.date);

  final String date;
  final double smiling;
}