import 'dart:async';

import 'package:geolocator/geolocator.dart';

class Geolocator{
  StreamSubscription<Position> _positionStream;
  GeolocatorPlatform _gp = GeolocatorPlatform.instance;
  Position position;

  StreamSubscription<Position> getGeolocatorStream(Function setState){
    return _gp
        .getPositionStream(desiredAccuracy: LocationAccuracy.high, distanceFilter: 10, timeInterval: 10)
        .listen((persona) { });
  }

}
