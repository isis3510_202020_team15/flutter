
import 'dart:async';
import 'dart:io';
// import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:image_picker/image_picker.dart';
//
class FaceRecognition{
  FirebaseVision _fb_vision = FirebaseVision.instance;
  Future<File> pickFromGallery () async {
    final File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    return  imageFile;
  }
//
  Future<File> pickFromCamera() async {
    final imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    return imageFile;
  }

}