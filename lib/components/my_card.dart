import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class MyCard extends StatelessWidget {
  final String name;
  final String subtitle;
  final double width;
  final bool open;
  final Function callback;
  const MyCard({this.name, this.subtitle, this.width, this.open, this.callback});

  @override
  Widget build(BuildContext context) {
    return  Container(
      width: width,

      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Colors.deepPurpleAccent,
        elevation: 10,
        child: Container(
            decoration: BoxDecoration(
        image: DecorationImage(
        image: CachedNetworkImageProvider("https://cdn.pixabay.com/photo/2016/11/29/05/45/astronomy-1867616__340.jpg"),
        fit: BoxFit.cover,
      ),
    ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.album, size: 70),
                title: Text(name, style: TextStyle(color: Colors.white)),
                subtitle: Text(subtitle, style: TextStyle(color: Colors.white)),
              ),
              open? ButtonTheme.bar(
                child: ButtonBar(
                  children: <Widget>[
                    FlatButton(
                      child: const Text('Open', style: TextStyle(color: Colors.white)),
                      onPressed: callback,
                    ),
                  ],
                ),
              ): Container(),
            ],
          ),
        ),
      ),
    );
  }
}
