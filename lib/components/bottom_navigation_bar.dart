import 'package:flutter/material.dart';

class BottomNavigationBarComponent extends StatefulWidget {
  BottomNavigationBarComponent({Key key}) : super(key: key);

  @override
  _BottomNavigationBarComponentState createState() => _BottomNavigationBarComponentState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _BottomNavigationBarComponentState extends State<BottomNavigationBarComponent> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Business',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Activities'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            title: Text('Cart'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue,
        onTap: _onItemTapped,
      );
  }
}
